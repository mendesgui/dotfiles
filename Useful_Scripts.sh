#!/bin/bash
while  [ $opcao!=0 ]
do
	clear
	echo ======= Digite o Número da função a ser utilizada =======
	echo ====== 1- [Instalar] Pacotes *do meu readme*      
	echo ====== 2- [Update] .Xresources                    
	echo ====== 3- [Instalar] i3-gaps Dependencias  
	echo ====== 4- [Instalar] Redshift   
    echo ________________________________________
    echo ========== Para sair aperte 0 ========== 
    echo Digite a Opcao
   
    read opcao

	case $opcao in
		"1" ) clear
		      sudo apt-get update 	
			  sudo apt-get upgrade -y 
			  sudo apt-get install tlp git compton rxvt-unicode arandr vlc nitrogen unzip unrar adb fastboot neofetch pavucontrol suckless-tools
			  read -rsp $'Pressione qualquer tecla...\n' -n1 key ;;
		
	
		"2" ) xrdb -merge ~/.Xresources ;;
	
		"3" ) echo instalando dependencias
			   sudo apt install libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev libxcb-icccm4-dev libyajl-dev libstartup-notification0-dev libxcb-randr0-dev libev-dev libxcb-cursor-dev libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev xutils-dev autoconf
	
		"4" ) sudo add-apt-repository ppa:dobey/redshift-daily
			   sudo apt-get update
			   sudo apt-get install redshift-gtk ;;
		
		"0" ) clear && break ;;
	
	esac
done
