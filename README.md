# Arquivos .config para o meu Setup
## Os pacotes utilizados são

* i3-gaps [Tutorial](https://benjames.io/2017/09/03/installing-i3-gaps-on-ubuntu-16-04/)
* polybar [Tutorial](https://linuxhint.com/status-bars-polybar-linux/)
* compton (Baixar e colocar o compton.conf na pasta .config)
* urxvt-unicode (Criar arquivo .Xresources e copiar a config lá)(copiar o conteúdo do .bashrc para a do sistema)
* Lock [Tutorial](http://plankenau.com/blog/post/gaussianlock)
* Font Awesome [Link](https://fontawesome.com/get-started)

*Todos as dependências podem ser instaladas com o Useful_Scripts.sh*